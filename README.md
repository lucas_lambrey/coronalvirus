# coronalvirus

Python script that turns all consonants in a word into coronal consonants. A word in orthographic form is transcripted into SAMPA, coronalized, and transcripted back to orthographic form.
Transcriptions use neural networks trained on the GLAWI corpus.

## Motivation
The motivation behind this project is simply a dark sense of humor and the need to mess around with neural networks.

## Description
The project's goal is to simulate a "coronal virus" that affects a word's consonants. 
By that, I mean that all consonants in a word are changed into the closest [coronal consonants](https://en.wikipedia.org/wiki/Coronal_consonant) (according to articulatory phonetics).

This modification impacts only the consonant's point of articulation, not its mode.

| Word (en) | Standard ortho (fr)   | Standard SAMPA (fr)   | Coronal SAMPA (fr)    | Coronal ortho (fr)    |
|-----------|-----------------------|-----------------------|-----------------------|-----------------------|
| apple     | pomme                 | pOm                   | tOn                   | tonne?                |
| dog       | chien                 | SjE~                  | SjE~                  | chien                 |
| ball      | balle                 | bal                   | dal                   | dalle?                |
| true      | vrai                  | vRE                   | srE                   | srai?                 |

### Project structure
The project contains several folders:

- `models`: contains the different neural network models
- `model training`: contains all files used to create the dataset, train and evaluate models
- `scripts`: scripts used in order to coronalize words

### Neural encoders (Unfinished)
Description to be done.

## Installation
Download the project.

## Usage (Unfinished)
Run the `coronalvirus.py` file.

## Contributing
As this project is not a serious application, I did not plan anything for people to contribute.

## Authors and acknowledgment
I, Lucas LAMBREY, am the sole author of this project.

## License
Feel free to reuse the models or the code in this project. You just need to cite me:
Lambrey, L. (2022). Orthographic to SAMPA and SAMPA to orthographic neural encoders. https://gitlab.com/lucas_lambrey/coronalvirus  
